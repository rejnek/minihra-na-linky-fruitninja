\babel@toc {czech}{}\relax 
\contentsline {section}{\numberline {1}O dokumentu}{2}{section.1}%
\contentsline {section}{\numberline {2}Výběr minihry z analýzy}{3}{section.2}%
\contentsline {section}{\numberline {3}Popis minihry Piano}{3}{section.3}%
\contentsline {subsection}{\numberline {3.1}Zodpovězení nejasností}{4}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2}Časová náročnost}{5}{subsection.3.2}%
\contentsline {section}{\numberline {4}Architektura hry}{5}{section.4}%
\contentsline {subsection}{\numberline {4.1}Počítání score}{5}{subsection.4.1}%
\contentsline {section}{\numberline {5}Mobilní aplikace}{5}{section.5}%
\contentsline {subsection}{\numberline {5.1}Uživatelské rozhraní}{6}{subsection.5.1}%
\contentsline {subsubsection}{\numberline {5.1.1}Menu}{7}{subsubsection.5.1.1}%
\contentsline {subsubsection}{\numberline {5.1.2}Tutorial}{7}{subsubsection.5.1.2}%
\contentsline {subsubsection}{\numberline {5.1.3}Lobby}{7}{subsubsection.5.1.3}%
\contentsline {subsubsection}{\numberline {5.1.4}Game}{7}{subsubsection.5.1.4}%
\contentsline {subsubsection}{\numberline {5.1.5}Score}{7}{subsubsection.5.1.5}%
\contentsline {subsubsection}{\numberline {5.1.6}PopUp}{7}{subsubsection.5.1.6}%
\contentsline {subsection}{\numberline {5.2}Počítání score v aplikaci}{7}{subsection.5.2}%
\contentsline {subsection}{\numberline {5.3}Komunikace se serverem}{7}{subsection.5.3}%
\contentsline {section}{\numberline {6}Serverová aplikace}{8}{section.6}%
\contentsline {subsection}{\numberline {6.1}Připojení do hry}{8}{subsection.6.1}%
\contentsline {subsection}{\numberline {6.2}Vygenerování hry}{8}{subsection.6.2}%
\contentsline {subsection}{\numberline {6.3}Vygenerování a poslání obrazu}{9}{subsection.6.3}%
