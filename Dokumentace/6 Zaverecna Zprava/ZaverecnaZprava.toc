\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {czech}{}\relax 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1}Úvod}{1}{section.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.1}Co jsou Linky?}{1}{subsection.1.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2}Analýza řešeného problému}{1}{section.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1}Popis úkolu}{1}{subsection.2.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.1.1}Popis použití z pohledu uživatele}{2}{subsubsection.2.1.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.1.2}Herní scénáře}{2}{subsubsection.2.1.2}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\numberline {2.1.2.1}Beat Saber / Fruit Ninja}{2}{paragraph.2.1.2.1}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\numberline {2.1.2.2}Tetris}{3}{paragraph.2.1.2.2}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\numberline {2.1.2.3}Klikačka}{4}{paragraph.2.1.2.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.1.3}Popis použití z pohledu správce Linek}{4}{subsubsection.2.1.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2}Popis stávajícího stavu}{5}{subsection.2.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.2.1}Provozní řád Linek}{5}{subsubsection.2.2.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.2.2}Zobrazování na Linkách}{5}{subsubsection.2.2.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.2.3}Uživatelský účet, simulátor}{5}{subsubsection.2.2.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3}Popis výběru prostředků vhodných pro řešení projektu}{5}{subsection.2.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.3.1}Logika aplikace}{5}{subsubsection.2.3.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.3.2}Nástroj pro vývoj mobilní aplikace}{6}{subsubsection.2.3.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.3.3}Server pro komunikaci s LinkyAPI}{7}{subsubsection.2.3.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.4}Popis výběru varianty řešení a výstupů}{7}{subsection.2.4}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.5}Stanovení dílčích úkolů}{7}{subsection.2.5}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.5.1}Dokumentace minihry}{7}{subsubsection.2.5.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.5.2}Program na simulátoru}{8}{subsubsection.2.5.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.5.3}Program s dynamickým UI}{8}{subsubsection.2.5.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.5.4}Program na Linkách}{8}{subsubsection.2.5.4}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3}Popis vlastního řešení maturitní práce}{8}{section.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1}1. výstup}{8}{subsection.3.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.1.1}Popis odvedené práce}{8}{subsubsection.3.1.1}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\numberline {3.1.1.1}Výběr minihry}{8}{paragraph.3.1.1.1}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\numberline {3.1.1.2}Popis minihry Piano}{9}{paragraph.3.1.1.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.1.2}Podoba po 1. výstupu}{12}{subsubsection.3.1.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2}2. výstup}{12}{subsection.3.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.2.1}Popis odvedené práce}{12}{subsubsection.3.2.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.2.2}Slepé uličky}{12}{subsubsection.3.2.2}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\numberline {3.2.2.1}Server}{12}{paragraph.3.2.2.1}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\numberline {3.2.2.2}Mobilní aplikace}{13}{paragraph.3.2.2.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.2.3}Podoba po 2. výstupu}{14}{subsubsection.3.2.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3}3. výstup}{14}{subsection.3.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.3.1}Popis odvedené práce}{14}{subsubsection.3.3.1}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\numberline {3.3.1.1}Přístup k API}{14}{paragraph.3.3.1.1}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\numberline {3.3.1.2}Online hosting}{15}{paragraph.3.3.1.2}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\numberline {3.3.1.3}Zobrazování na Simulátoru/Linkách}{16}{paragraph.3.3.1.3}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\numberline {3.3.1.4}Generování mapy}{16}{paragraph.3.3.1.4}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.3.2}Podoba po 3. výstupu}{16}{subsubsection.3.3.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.4}4. výstup}{17}{subsection.3.4}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.4.1}Popis odvedené práce}{17}{subsubsection.3.4.1}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\numberline {3.4.1.1}Doména}{17}{paragraph.3.4.1.1}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\numberline {3.4.1.2}Nové logo, favicon}{17}{paragraph.3.4.1.2}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\numberline {3.4.1.3}Mapa odesílaná na Linky}{18}{paragraph.3.4.1.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.4.2}Vylepšení aplikace}{18}{subsubsection.3.4.2}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\numberline {3.4.2.1}Screen Lock}{19}{paragraph.3.4.2.1}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\numberline {3.4.2.2}Vylepšení Lobby, Score, Návodu}{19}{paragraph.3.4.2.2}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\numberline {3.4.2.3}Ukládání přezdívky}{20}{paragraph.3.4.2.3}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\numberline {3.4.2.4}Klikání na Pianu}{20}{paragraph.3.4.2.4}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.4.3}Podoba po 4. výstupu}{20}{subsubsection.3.4.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4}Kód}{20}{section.4}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5}Závěr}{21}{section.5}%
