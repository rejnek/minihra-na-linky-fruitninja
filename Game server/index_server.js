// -- test kresleni
// import {Print_frame} from "./print_frame";
// Print_frame.Run();
// --
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var path = require('path');
var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var _a = require("./lobby_server.js"), Lobby = _a.Lobby, Player = _a.Player;
var Comunicator = require("./linky_api").Comunicator;
var Game = require("./game_server.js").Game;
var port = process.env.PORT || 3010;
app.use(express.static(__dirname + '/Web app'));
// nastaveni promenych
var nconf = require('nconf');
nconf.file({ file: 'config.json' });
var debugMode = nconf.get('debug_mode');
var gameDuration = nconf.get('duration');
var gameSpeed = nconf.get('speed');
var noteSize = nconf.get('note_size');
var screenRefresh = nconf.get('refresh');
new Lobby();
new Game();
new Comunicator(debugMode);
app.get('/', function (req, res) {
    res.sendFile(__dirname + '/Web app/index.html');
});
io.on('connection', function (socket) {
    console.log('user connected: ' + socket.id);
    socket.on('login', function (msg) {
        Lobby.instance.AddPlayer(new Player(socket.id, msg.toString()));
        io.emit('queue', Lobby.instance.QueueArray());
        testReady();
        // console.log('login: ' + socket.id);
    });
    socket.on('signOut', function () {
        Lobby.instance.RemovePlayer(socket.id);
        Game.instance.RemovePlayer(socket.id);
        io.emit('queue', Lobby.instance.QueueArray());
        testReady();
    });
    socket.on('ready', function (msg) {
        Lobby.instance.ReadyPlayer(socket.id);
        io.emit('queue', Lobby.instance.QueueArray());
        testReady();
        console.log(Lobby.instance.queue);
    });
    socket.on('disconnect', function () {
        Lobby.instance.RemovePlayer(socket.id);
        Game.instance.RemovePlayer(socket.id);
        console.log(socket.id + " disconnected");
        io.emit('queue', Lobby.instance.QueueArray());
        testReady();
    });
    socket.on('score', function (msg) {
        for (var _i = 0, _a = Game.instance.players; _i < _a.length; _i++) {
            var player = _a[_i];
            io.to(player.idPlayer).emit('scoreBoardEntry', msg);
        }
    });
    function testReady() {
        if (Lobby.instance.EverybodyReady() && Game.instance.players.length == 0) {
            Game.instance.GenerateMap(gameSpeed, noteSize, gameDuration);
            Game.instance.players = Array.from(Lobby.instance.queue);
            for (var _i = 0, _a = Lobby.instance.queue; _i < _a.length; _i++) {
                var player = _a[_i];
                io.to(player.idPlayer).emit('startGame', Game.instance);
            }
            Lobby.instance.ClearLobby();
            endGame(gameDuration * 1000).then(function (r) { return null; });
            // posilani obrazu na Linky -------
            Comunicator.instance.CreateToken()
                .then(function () {
                return Comunicator.instance.PlayLinky(Game.instance.map, Game.instance.speed, screenRefresh);
            });
            // --------------------------------
        }
        else {
            Comunicator.instance.Stop();
        }
    }
    function endGame(duration) {
        return __awaiter(this, void 0, void 0, function () {
            var _i, _a, player;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0: return [4 /*yield*/, new Promise(function (resolve) {
                            setTimeout(resolve, duration);
                        })];
                    case 1:
                        _b.sent();
                        for (_i = 0, _a = Game.instance.players; _i < _a.length; _i++) {
                            player = _a[_i];
                            io.to(player.idPlayer).emit('endGame');
                        }
                        Comunicator.instance.Stop();
                        setTimeout(function () {
                            Game.instance.players = [];
                        }, 3000); // score musi byt odeslane do 3s
                        return [2 /*return*/];
                }
            });
        });
    }
});
http.listen(port, function () {
    console.log("Socket.IO server running at http://localhost:".concat(port, "/"));
});
