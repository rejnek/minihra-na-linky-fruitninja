const {Comunicator} = require("./linky_api");

class PianoMap {
    public static Instance: PianoMap;

    public speed: number; // rychlost padani n segmentu za s
    public gameLength: number; // delky hry v s

    public noteSize: number;

    public mobileMap: Note[];
    public map: any[];

    constructor(speed: number, gameLength: number, noteSize: number) {
        PianoMap.Instance = this;

        this.speed = speed;
        this.gameLength = gameLength;
        this.noteSize = noteSize;

        this.mobileMap = [];
        this.map = new Array(5); // sloupce
        for (let i = 0; i < this.map.length; i += 1) {
            this.map[i] = new Array(this.speed * this.gameLength); // radky
            this.map[i].fill('000000');
        }

        // Naplneni mapy

        let min = 50;
        let max = 100;
        let block = 0 + PianoMap.RandomInteger(min, max);

        while (block + noteSize < this.map[0].length) {
            let key = PianoMap.RandomInteger(0, 4);

            this.map[key].fill('FF00FF', block, block + this.noteSize);
            this.mobileMap.push(new Note("k" + (key + 1), block / speed, (block + this.noteSize) / speed));
            block = block + noteSize + PianoMap.RandomInteger(min, max);
        }

        // konec naplnovani

        // console.log(this.mobileMap);
    }

    // int min (included) and max (included):
    static RandomInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
}

class Note {
    public code: string;
    public hitBegin: Number;
    public hitEnd: Number;

    public constructor(code: string, hitBegin: Number, hitEnd: Number) {
        this.code = code;
        this.hitBegin = hitBegin;
        this.hitEnd = hitEnd;
    }
}


let p = new PianoMap(8, 60, 45);
let c = new Comunicator();
c.CreateToken()
    .then(() =>
        c.PlayDebug(p.map, p.speed, 1)
    );
