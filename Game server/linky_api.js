var axios = require('axios');
var nconf = require('nconf');
var Comunicator = /** @class */ (function () {
    function Comunicator(debugMode) {
        Comunicator.instance = this;
        this.debugMode = debugMode;
        nconf.file({ file: 'config.json' });
    }
    Comunicator.prototype.CreateToken = function () {
        var _this = this;
        return axios.post('https://linky.fel.cvut.cz/Api/v1/Token/Create', {
            user_id: 'e811c1a8-d6a8-436f-9778-177f955a1b45',
            user_secret: '90b72d25-10fb-4b7b-841f-5af2240924aa',
            debug: this.debugMode,
            fps: 30
        }).then(function (res) {
            _this.aToken = res.data.access_token;
            _this.RefreshToken(res.data.refresh_token);
            setInterval(function () { return _this.RefreshToken(res.data.refresh_token); }, 150 * 1000);
            console.log("CreateToken: " + res.status);
            console.log("aToken: " + _this.aToken);
        }).catch(function (error) {
            console.error(error);
        });
    };
    Comunicator.prototype.RefreshToken = function (token) {
        axios.post('https://linky.fel.cvut.cz/Api/v1/Token/Refresh', {
            refresh_token: token
        }).then(function (res) {
            console.log("RefreshToken: " + res.status);
        }).catch(function (error) {
            console.error(error);
        });
    };
    Comunicator.prototype.Play = function (array) {
        axios.post(nconf.get('post_url'), {
            access_token: this.aToken,
            frames: array,
            clear_buffer: true
        }).then(function (res) {
            console.log("Play: " + res.status);
        }).catch(function (error) {
            console.error("Play: ERROR!!!");
        });
        // console.dir(array, {'maxArrayLength': null});
    };
    Comunicator.prototype.PlayLinky = function (array, speed, refresh) {
        var _this = this;
        var time = 0;
        //posilej kazdych Xs novy obraz
        this.interval = setInterval(function () {
            time += refresh;
            _this.PlaySection(array, time, speed);
        }, refresh * 1000);
    };
    Comunicator.prototype.PlaySection = function (array, time, speed) {
        var columns = new Array(5);
        // pro kazdy sloupec
        for (var j = 0; j < columns.length; j += 1) {
            // test jestli nejsme na konci
            if (time * speed + 204 > array[j].length) {
                columns[j] = new Array(204).fill('000000');
            }
            else {
                var begin = (time * speed) | 0;
                columns[j] = (array[j].slice(begin, begin + 204)).reverse();
                if (columns[j].length != 204)
                    console.log("Length Error PlaySection()");
            }
        }
        this.Play([columns]);
    };
    Comunicator.prototype.Stop = function () {
        this.ClearScreen();
        clearInterval(this.interval);
    };
    Comunicator.prototype.ClearScreen = function () {
        var a = new Array(1); // snimky
        for (var i = 0; i < a.length; i += 1) {
            a[i] = new Array(5); // sloupce
            for (var j = 0; j < a[i].length; j += 1) {
                a[i][j] = new Array(204).fill('000000'); // radky
            }
        }
        this.Play(a);
    };
    return Comunicator;
}());
module.exports = { Comunicator: Comunicator };
