import * as PImage from "pureimage"
import * as fs from 'fs'
const {Game} = require("./game_server.js");

class Print_frame{

    public static Run(){
        let speed = 30;

        let map = this.GenerateMap(speed, 60, 600);
        this.PlayMap(map, speed, 10, 100, 10)
    }

    public static GenerateMap(speed: number, noteSize:number, length:number){
        new Game();
        Game.instance.GenerateMap(speed, noteSize, length);
        return Game.instance.map;
    }

    private static ArrayApiFine(array: string[][][]){
        console.log("Length test")
        for(let frame of array){
            if(frame.length != 5) console.log("ERROR - 5");
            for(let c of frame){
                if(c.length != 204) console.log("ERROR - 204");
            }
        }
    }

    public static async PlayMap(array: string[][], speed: number, refresh: number, startTime: number, fps:number) {
        console.log("Delka mapy: " + array[0].length)
        let time = startTime;

        // vygeneruj vsechny snimky co budou potreba
        let frames = new Array(refresh * fps);

        // pro kazdy snimek
        for (let i = 0; i < frames.length; i += 1) {
            frames[i] = new Array(5);
            // pro kazdy sloupec
            for (let j = 0; j < frames[i].length; j += 1) {

                // test jestli nejsme na konci
                if (time * speed + 204 > array[j].length) {
                    frames[i][j] = new Array(204).fill('000000');
                } else {
                    frames[i][j] = (array[j].slice(time * speed, time * speed + 204)).reverse();
                }
            }
            time += 1 / fps;
        }

        this.CreateAll("testImages", frames);
    }

    public static CreateAll(path, frames: string[][][]){
        this.ArrayApiFine(frames);

        for (let frame of frames){
            this.CreateImage(path, frame);
        }

        // sirka sloupce 4px
        const img1 = PImage.make(20, 204, "")
        const ctx = img1.getContext('2d');
        ctx.fillStyle = 'magenta';
        ctx.fillRect(0, 0, 20, 204);

        //write to 'out.png'
        PImage.encodePNGToStream(img1, fs.createWriteStream(path + '/out' + Date.now() + '.png')).then(() => {
            // console.log("wrote out the png file to out.png");
        }).catch((e)=>{
            console.log("there was an error writing");
        });
    }

    public static CreateImage(path, frame){

        // sirka sloupce 4px
        const img1 = PImage.make(20, 204, "")
        const ctx = img1.getContext('2d');
        ctx.fillStyle = 'white';
        ctx.fillRect(0, 0, 20, 204);

        ctx.fillStyle = 'magenta';
        // column
        for(let i = 0; i < frame.length; i+=1){
            // pixel
            for(let j = 0; j < frame[i].length; j+=1){
                if(frame[i][j] != '000000'){
                    ctx.fillRect(i * 4, j, 4, 1);
                }
            }
        }

        //write to 'out.png'
        PImage.encodePNGToStream(img1, fs.createWriteStream(path + '/out' + Date.now() + '.png')).then(() => {
            // console.log("wrote out the png file to out.png");
        }).catch((e)=>{
            console.log("there was an error writing");
        });
    }
}
export {Print_frame}