var Comunicator = require("./linky_api").Comunicator;
var PianoMap = /** @class */ (function () {
    function PianoMap(speed, gameLength, noteSize) {
        PianoMap.Instance = this;
        this.speed = speed;
        this.gameLength = gameLength;
        this.noteSize = noteSize;
        this.mobileMap = [];
        this.map = new Array(5); // sloupce
        for (var i = 0; i < this.map.length; i += 1) {
            this.map[i] = new Array(this.speed * this.gameLength); // radky
            this.map[i].fill('000000');
        }
        // Naplneni mapy
        var min = 50;
        var max = 100;
        var block = 0 + PianoMap.RandomInteger(min, max);
        while (block + noteSize < this.map[0].length) {
            var key = PianoMap.RandomInteger(0, 4);
            this.map[key].fill('FF00FF', block, block + this.noteSize);
            this.mobileMap.push(new Note("k" + (key + 1), block / speed, (block + this.noteSize) / speed));
            block = block + noteSize + PianoMap.RandomInteger(min, max);
        }
        // konec naplnovani
        // console.log(this.mobileMap);
    }
    // int min (included) and max (included):
    PianoMap.RandomInteger = function (min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    };
    return PianoMap;
}());
var Note = /** @class */ (function () {
    function Note(code, hitBegin, hitEnd) {
        this.code = code;
        this.hitBegin = hitBegin;
        this.hitEnd = hitEnd;
    }
    return Note;
}());
var p = new PianoMap(8, 60, 45);
var c = new Comunicator();
c.CreateToken()
    .then(function () {
    return c.PlayDebug(p.map, p.speed, 1);
});
