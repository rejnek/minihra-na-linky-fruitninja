class Lobby {
    constructor() {
        this.queue = [];
    }

    AddPlayer(newPlayer) {
        for (let i = 0; i < this.queue.length; i += 1) {
            if (this.queue[i].idPlayer === newPlayer.idPlayer) {
                this.queue[i] = newPlayer;
                return;
            }
        }
        this.queue.push(newPlayer);
    }

    RemovePlayer(idPlayer) {
        for (let i = 0; i < this.queue.length; i += 1) {
            if (this.queue[i].idPlayer === idPlayer) {
                this.queue.splice(i, i);
                return;
            }
        }
    }

    QueueString(){
        let result = "";
        for (let i = 0; i < this.queue.length; i += 1) {
            result += this.queue[i].nickname + "\n";
        }
        return result;
    }
}

class Player {
    constructor(idPlayer, nickname) {
        this.idPlayer = idPlayer;
        this.nickname = nickname;
    }
}

module.exports = {Lobby, Player};
