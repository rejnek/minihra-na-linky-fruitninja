const app = require('express')();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const port = process.env.PORT || 3000;
const gs = require('./game_server.js');
const {Player} = require("./game_server");

let lobby = new gs.Lobby();


app.get('/', (req, res) => {
    res.sendFile(__dirname + './index.html');
});

io.on('connection', (socket) => {
    console.log('user connected: ' + socket.id)

    socket.on('ready', msg => {
        lobby.AddPlayer(new Player(socket.id, msg.toString()));
        io.emit('queue', lobby.QueueString());
        console.log(lobby.queue);
    });

    socket.on('disconnect', () => {
        console.log('user disconnected: ' + socket.id);
        lobby.RemovePlayer(socket.id);
    });
});

http.listen(port, () => {
    console.log(`Socket.IO server running at http://localhost:${port}/`);
});