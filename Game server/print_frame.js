"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Print_frame = void 0;
var PImage = require("pureimage");
var fs = require("fs");
var Game = require("./game_server.js").Game;
var Print_frame = /** @class */ (function () {
    function Print_frame() {
    }
    Print_frame.Run = function () {
        var speed = 30;
        var map = this.GenerateMap(speed, 60, 600);
        this.PlayMap(map, speed, 10, 100, 10);
    };
    Print_frame.GenerateMap = function (speed, noteSize, length) {
        new Game();
        Game.instance.GenerateMap(speed, noteSize, length);
        return Game.instance.map;
    };
    Print_frame.ArrayApiFine = function (array) {
        console.log("Length test");
        for (var _i = 0, array_1 = array; _i < array_1.length; _i++) {
            var frame = array_1[_i];
            if (frame.length != 5)
                console.log("ERROR - 5");
            for (var _a = 0, frame_1 = frame; _a < frame_1.length; _a++) {
                var c = frame_1[_a];
                if (c.length != 204)
                    console.log("ERROR - 204");
            }
        }
    };
    Print_frame.PlayMap = function (array, speed, refresh, startTime, fps) {
        return __awaiter(this, void 0, void 0, function () {
            var time, frames, i, j;
            return __generator(this, function (_a) {
                console.log("Delka mapy: " + array[0].length);
                time = startTime;
                frames = new Array(refresh * fps);
                // pro kazdy snimek
                for (i = 0; i < frames.length; i += 1) {
                    frames[i] = new Array(5);
                    // pro kazdy sloupec
                    for (j = 0; j < frames[i].length; j += 1) {
                        // test jestli nejsme na konci
                        if (time * speed + 204 > array[j].length) {
                            frames[i][j] = new Array(204).fill('000000');
                        }
                        else {
                            frames[i][j] = (array[j].slice(time * speed, time * speed + 204)).reverse();
                        }
                    }
                    time += 1 / fps;
                }
                this.CreateAll("testImages", frames);
                return [2 /*return*/];
            });
        });
    };
    Print_frame.CreateAll = function (path, frames) {
        this.ArrayApiFine(frames);
        for (var _i = 0, frames_1 = frames; _i < frames_1.length; _i++) {
            var frame = frames_1[_i];
            this.CreateImage(path, frame);
        }
        // sirka sloupce 4px
        var img1 = PImage.make(20, 204, "");
        var ctx = img1.getContext('2d');
        ctx.fillStyle = 'magenta';
        ctx.fillRect(0, 0, 20, 204);
        //write to 'out.png'
        PImage.encodePNGToStream(img1, fs.createWriteStream(path + '/out' + Date.now() + '.png')).then(function () {
            // console.log("wrote out the png file to out.png");
        }).catch(function (e) {
            console.log("there was an error writing");
        });
    };
    Print_frame.CreateImage = function (path, frame) {
        // sirka sloupce 4px
        var img1 = PImage.make(20, 204, "");
        var ctx = img1.getContext('2d');
        ctx.fillStyle = 'white';
        ctx.fillRect(0, 0, 20, 204);
        ctx.fillStyle = 'magenta';
        // column
        for (var i = 0; i < frame.length; i += 1) {
            // pixel
            for (var j = 0; j < frame[i].length; j += 1) {
                if (frame[i][j] != '000000') {
                    ctx.fillRect(i * 4, j, 4, 1);
                }
            }
        }
        //write to 'out.png'
        PImage.encodePNGToStream(img1, fs.createWriteStream(path + '/out' + Date.now() + '.png')).then(function () {
            // console.log("wrote out the png file to out.png");
        }).catch(function (e) {
            console.log("there was an error writing");
        });
    };
    return Print_frame;
}());
exports.Print_frame = Print_frame;
