var Lobby = /** @class */ (function () {
    function Lobby() {
        Lobby.instance = this;
        this.queue = [];
    }
    Lobby.prototype.ClearLobby = function () {
        this.queue = [];
    };
    Lobby.prototype.AddPlayer = function (newPlayer) {
        for (var i = 0; i < this.queue.length; i += 1) {
            if (this.queue[i].idPlayer === newPlayer.idPlayer) {
                var oldReady = this.queue[i].ready;
                this.queue[i] = newPlayer;
                this.queue[i].ready = oldReady;
                return;
            }
        }
        this.queue.push(newPlayer);
    };
    Lobby.prototype.RemovePlayer = function (idPlayer) {
        for (var i = 0; i < this.queue.length; i += 1) {
            if (this.queue[i].idPlayer === idPlayer) {
                console.log('removing: ' + this.queue[i].idPlayer);
                this.queue.splice(i, 1);
                return;
            }
        }
    };
    Lobby.prototype.ReadyPlayer = function (idPlayer) {
        for (var _i = 0, _a = this.queue; _i < _a.length; _i++) {
            var player = _a[_i];
            if (player.idPlayer === idPlayer) {
                player.ready = true;
                return;
            }
        }
    };
    Lobby.prototype.EverybodyReady = function () {
        var count = 0;
        for (var _i = 0, _a = this.queue; _i < _a.length; _i++) {
            var player = _a[_i];
            if (player.ready === true) {
                count += 1;
            }
        }
        return (count >= 5 || count == this.queue.length) && count != 0;
    };
    Lobby.prototype.QueueArray = function () {
        var result = [];
        for (var _i = 0, _a = this.queue; _i < _a.length; _i++) {
            var player = _a[_i];
            if (player.ready) {
                result.push([player.nickname, "ready"]);
            }
            else {
                result.push([player.nickname, "waiting"]);
            }
        }
        return result;
    };
    return Lobby;
}());
var Player = /** @class */ (function () {
    function Player(idPlayer, nickname) {
        this.idPlayer = idPlayer;
        this.nickname = nickname;
        this.ready = false;
    }
    return Player;
}());
module.exports = { Lobby: Lobby, Player: Player };
