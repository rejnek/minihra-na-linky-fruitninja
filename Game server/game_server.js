var Game = /** @class */ (function () {
    function Game() {
        Game.instance = this;
        this.mobileMap = [];
        this.players = [];
    }
    Game.prototype.GenerateMap = function (speed, noteSize, gameLength) {
        this.speed = speed;
        this.duration = gameLength;
        this.map = new Array(5); // sloupce
        for (var i = 0; i < this.map.length; i += 1) {
            this.map[i] = new Array(this.speed * gameLength); // radky
            this.map[i].fill('000000');
        }
        // Naplneni mapy
        var min = 0;
        var max = noteSize / 2;
        var block = 204;
        console.log(this.map[0].length);
        while (block + noteSize < this.map[0].length) {
            var key = Game.randomInteger(0, 4);
            this.map[key].fill('FF00FF', block, block + noteSize);
            this.mobileMap.push(new Note("k" + key, (block / this.speed) * 1000, ((block + noteSize) / this.speed) * 1000));
            block = block + noteSize + Game.randomInteger(min, max);
            console.log('FF00FF');
        }
        // konec naplnovani
        for (var i = 0; i < this.map.length; i += 1) {
            this.map[i] = this.map[i].concat(new Array(204).fill('000000'));
        }
        // console.log(this.map);
    };
    Game.prototype.RemovePlayer = function (idPlayer) {
        for (var i = 0; i < this.players.length; i += 1) {
            if (this.players[i].idPlayer === idPlayer) {
                console.log('removing: ' + this.players[i].idPlayer);
                this.players.splice(i, 1);
                return;
            }
        }
    };
    // float min (included) and max (not included):
    Game.randomNumber = function (min, max) {
        return Math.random() * (max - min) + min;
    };
    // int min (included) and max (included):
    Game.randomInteger = function (min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    };
    return Game;
}());
var Note = /** @class */ (function () {
    function Note(code, hitBegin, hitEnd) {
        this.code = code;
        this.hitBegin = hitBegin;
        this.hitEnd = hitEnd;
    }
    return Note;
}());
var Player = /** @class */ (function () {
    function Player() {
    }
    return Player;
}());
module.exports = { Game: Game, Note: Note };
