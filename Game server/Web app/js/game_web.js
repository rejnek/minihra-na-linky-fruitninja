var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var GameMobile = /** @class */ (function () {
    function GameMobile(txtTime, txtScore) {
        GameMobile.instance = this;
        this.active = false;
        this.score = 0;
        this.txtScore = txtScore;
        this.txtTime = txtTime;
    }
    GameMobile.prototype.start = function (game) {
        this.timeRemaining = game.duration * 1000;
        this.score = 0;
        this.active = true;
        this.duration = game.duration * 1000;
        this.startTime = Date.now();
        this.map = game.mobileMap;
        this.txtScore.textContent = this.score.toString();
        this.txtTime.textContent = Math.floor(this.timeRemaining / 1000) + "s";
    };
    GameMobile.prototype.keyPress = function (code) {
        var tFromStart = Date.now() - this.startTime;
        for (var _i = 0, _a = this.map; _i < _a.length; _i++) {
            var note = _a[_i];
            if (note.hitBegin < tFromStart && note.hitEnd > tFromStart && note.code == code) {
                this.score += 10;
                this.txtScore.textContent = this.score.toString();
                return;
            }
        }
        this.score -= 10;
        this.txtScore.textContent = this.score.toString();
    };
    GameMobile.prototype.updateTime = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!(this.timeRemaining > 1000 && this.active)) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.delay(1000)];
                    case 1:
                        _a.sent();
                        this.timeRemaining = this.duration - (Date.now() - this.startTime);
                        this.txtTime.textContent = Math.floor(this.timeRemaining / 1000) + "s";
                        return [3 /*break*/, 0];
                    case 2: return [2 /*return*/];
                }
            });
        });
    };
    GameMobile.prototype.delay = function (milliseconds) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve) {
                        setTimeout(resolve, milliseconds);
                    })];
            });
        });
    };
    return GameMobile;
}());
var Game = /** @class */ (function () {
    function Game() {
    }
    return Game;
}());
var Note = /** @class */ (function () {
    function Note() {
    }
    return Note;
}());
