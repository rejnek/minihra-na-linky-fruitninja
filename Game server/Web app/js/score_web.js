var ScoreBoard = /** @class */ (function () {
    function ScoreBoard(thisId, h1Place, liOther) {
        ScoreBoard.instance = this;
        this.thisId = thisId;
        this.entries = new Array();
        this.h1Place = h1Place;
        this.liOther = liOther;
    }
    ScoreBoard.prototype.AddScoreEntry = function (entry) {
        // vloz do pole
        // serad pole
        for (var i = 0; i <= this.entries.length; i += 1) {
            if (i == this.entries.length) {
                this.entries.push(entry);
                break;
            }
            if (entry.score > this.entries[i].score) {
                this.entries.splice(i, 0, entry);
                break;
            }
        }
        // vypis pole, updatni moji pozici
        this.liOther.innerHTML = '';
        for (var i = 0; i < this.entries.length; i += 1) {
            var p = this.entries[i];
            var li = document.createElement("li");
            if (p.idPlayer == this.thisId) {
                li.appendChild(document.createTextNode((p.nickname + " -> " + p.score)));
                li.style.fontWeight = "700";
                this.h1Place.innerHTML = (i + 1) + ".";
            }
            else {
                li.appendChild(document.createTextNode(p.nickname + " -> " + p.score));
            }
            this.liOther.appendChild(li);
        }
    };
    ScoreBoard.prototype.AddScoreEntryStr = function (playerID, nickname, score) {
        this.AddScoreEntry(new ScoreEntry(playerID, nickname, parseInt(score)));
    };
    ScoreBoard.prototype.reset = function () {
        this.entries = new Array();
    };
    return ScoreBoard;
}());
var ScoreEntry = /** @class */ (function () {
    function ScoreEntry(idPlayer, name, score) {
        this.idPlayer = idPlayer;
        this.nickname = name;
        this.score = score;
    }
    return ScoreEntry;
}());
