function createRipple(event) {
    const button = event.currentTarget;

    const circle = document.createElement("span");
    const diameter = Math.max(button.clientWidth, button.clientHeight);
    const radius = diameter / 2;

    var evt = (typeof event.originalEvent === 'undefined') ? event : event.originalEvent;
    var touch = evt.touches[0] || evt.changedTouches[0];
    console.log(touch.pageX);
    console.log(touch.pageY);


    circle.style.width = circle.style.height = `${diameter}px`;
    circle.style.left = `${touch.pageX - button.offsetLeft - radius}px`;
    circle.style.top = `${touch.pageY - button.offsetTop - radius}px`;
    circle.classList.add("ripple");

    /*
    const ripple = button.getElementsByClassName("ripple")[0];

    if (ripple) {
        ripple.remove();
    }
     */

    navigator.vibrate(100); // vibrate for 200ms
    button.appendChild(circle);
}

const buttons = document.getElementsByClassName("key");
for(let i = 0; i < buttons.length; i+= 1){
    buttons[i].addEventListener("touchstart", createRipple);
}