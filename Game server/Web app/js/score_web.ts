class ScoreBoard {
    public static instance: ScoreBoard;
    public thisId: string;
    private entries: Array<ScoreEntry>;
    private h1Place: HTMLHeadingElement;
    private liOther: HTMLOListElement;

    constructor(thisId: string, h1Place: HTMLHeadingElement, liOther: HTMLOListElement) {
        ScoreBoard.instance = this;
        this.thisId = thisId;
        this.entries = new Array<ScoreEntry>();
        this.h1Place = h1Place;
        this.liOther = liOther;
    }

    public AddScoreEntry(entry: ScoreEntry): void {
        // vloz do pole
        // serad pole
        for (let i = 0; i <= this.entries.length; i += 1) {
            if (i == this.entries.length) {
                this.entries.push(entry);
                break;
            }

            if (entry.score > this.entries[i].score) {
                this.entries.splice(i, 0, entry);
                break;
            }
        }

        // vypis pole, updatni moji pozici
        this.liOther.innerHTML = '';
        for (let i = 0; i < this.entries.length; i += 1) {
            let p = this.entries[i];
            let li = document.createElement("li");

            if (p.idPlayer == this.thisId) {
                li.appendChild(document.createTextNode((p.nickname +  " -> " + p.score)));
                li.style.fontWeight = "700";

                this.h1Place.innerHTML = (i + 1) + ".";
            } else {
                li.appendChild(document.createTextNode(p.nickname + " -> " + p.score));
            }
            this.liOther.appendChild(li);
        }
    }

    public AddScoreEntryStr(playerID: string, nickname: string, score: string): void {
        this.AddScoreEntry(new ScoreEntry(playerID, nickname, parseInt(score)));
    }

    public reset():void{
        this.entries = new Array<ScoreEntry>();
    }

}

class ScoreEntry {
    public idPlayer: string;
    public nickname: string;
    public score: number;

    constructor(idPlayer: string, name: string, score: number) {
        this.idPlayer = idPlayer;
        this.nickname = name;
        this.score = score;
    }
}