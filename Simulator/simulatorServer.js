const express = require('express');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const portSocket = 3030;

app.use(express.static(__dirname + '/'));
app.use(express.json());

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');
});

app.post('/play', async(req, res) => {
    console.log(req.body);
    io.emit('play', req.body.frames[0]);
    res.status(200).send('Everything fine.');
})

io.on('connection', (socket) => {
    console.log('user connected: ' + socket.id);
});

http.listen(portSocket, () => {
    console.log(`Socket.IO server running at http://localhost:${portSocket}/`);
});